import UserRepository from "../repository/user.repository";
import UserResolver from "../resolver/user.resolver";
import UserService from "../service/user.service";
import OrderRepository from "../repository/order.repository";
import OrderResolver from "../resolver/order.resolver";
import OrderService from "../service/order.service";

/**
 * Initiate root value for graphql.
 * This will also inject dependencies for all related classes
 * @returns root object
 */
export default function initRoot() {
  const userRepository = new UserRepository();
  const orderRepository = new OrderRepository();

  const userService = new UserService(userRepository, orderRepository);
  const orderService = new OrderService(orderRepository, userRepository);

  const orderResolver = new OrderResolver(orderService);
  const userResolver = new UserResolver(userService);

  return {
    hello: userResolver.getUser,
    createUser: userResolver.createUser.bind(userResolver),
    updateUser: userResolver.updateUser.bind(userResolver),
    getUser: userResolver.getUser.bind(userResolver),
    createOrder: orderResolver.createOrder.bind(orderResolver),
    getOrder: orderResolver.getOrder.bind(orderResolver),
    getOrders: orderResolver.getOrders.bind(orderResolver),
  };
}
