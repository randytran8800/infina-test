import { CreateOrderDTO, OrderResult } from "types/dto";
import { OrderModel } from "types/model";
import { IOrderRepository, IUserRepository } from "types/repository";
import { IOrderService } from "types/service";
import { CreateOrderValidator } from "../validator/order.validator";
import { randomIntFromInterval } from "../util/random";

export default class OrderService implements IOrderService {
  private repo: IOrderRepository;
  private userRepo: IUserRepository;

  constructor(orderRepository: IOrderRepository, userRepo: IUserRepository) {
    this.repo = orderRepository;
    this.userRepo = userRepo;
  }

  /**
   * Create order
   * @param dto - dto to create order
   * @returns Order result
   * @throws {@link Error} When input is invalid or user not found
   */
  async createOrder(dto: CreateOrderDTO): Promise<OrderResult> {
    const { error } = CreateOrderValidator.validate(dto);
    if (error) {
      throw new Error("Invalid input: " + error.message);
    }

    const existedUser = await this.userRepo.findById(dto.user);
    if (!existedUser) throw new Error("User not found");

    let code;
    do {
      code = OrderService.generateCode();
    } while (!(await this.isValidCode(code)));

    const model: OrderModel = {
      code,
      user: dto.user,
      interest_rate: dto.interest_rate,
      amount: dto.amount,
    };
    const created = await this.repo.create(model);
    return this.adaptOrderResult(created);
  }

  /**
   * Get order by id
   * @param id - order's id
   * @returns Order result
   * @throws {@link Error} When id is not exist
   */
  async getOrder(id: string): Promise<OrderResult> {
    const existed = await this.assertOrderById(id);
    return this.adaptOrderResult(existed);
  }

  /**
   * Get all orders by specific user
   * @param userId - user's id
   * @returns Order result list
   */
  async getOrdersByUser(userId: string): Promise<OrderResult[]> {
    const orders = await this.repo.findAllByUser(userId);
    return orders.map(this.adaptOrderResult);
  }

  /**
   * Check if code is valid
   * @param code - code to check
   * @returns a boolean indicate whether code is valid or not
   */
  async isValidCode(code: string): Promise<boolean> {
    const isCodeExisted = await this.repo.findByCode(code);
    return !isCodeExisted;
  }

  /**
   * Adapt order model to order result
   * @param model - order model
   * @returns Order result
   */
  private adaptOrderResult(model: OrderModel): OrderResult {
    const result: OrderResult = {
      ...model,
      _id: model._id || "",
      accrued_amount: OrderService.calculateAccruedAmount(
        model.amount,
        model.interest_rate
      ),
    };
    return result;
  }

  /**
   * Assert and get order by id
   * @param id - order's
   * @returns Order Model
   * @throws {@link Error} when order id is not existed
   */
  private async assertOrderById(id: string): Promise<OrderModel> {
    const existedUserOrder = await this.repo.findById(id);
    if (!existedUserOrder) {
      throw new Error("Order not found");
    }
    return existedUserOrder;
  }

  /**
   * Generate code by specific length
   * @param length - length of code
   * @returns code
   */
  private static generateCode(length: number = 5): string {
    let code = "";
    for (let i = 0; i < length; i++) {
      code += randomIntFromInterval(0, 9).toString();
    }
    return code;
  }

  /**
   * Calculate accrued amount in the next 12 month
   * @param amount - amount to calculate
   * @param interest_rate - interest rate per year
   * @returns list of accrued amount in next 12 month
   */
  private static calculateAccruedAmount(
    amount: number,
    interest_rate: number
  ): number[] {
    const accruedAmount = Array(12).fill(0);
    let lastAccruedAmount = amount;
    for (let i = 0; i < 12; i++) {
      accruedAmount[i] = Math.round(
        lastAccruedAmount * (1 + interest_rate / 12)
      );
      lastAccruedAmount = accruedAmount[i];
    }
    return accruedAmount;
  }
}
