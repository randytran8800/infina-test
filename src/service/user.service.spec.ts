jest.mock("../repository/user.repository");
jest.mock("../repository/order.repository");

import UserService from "./user.service";
import UserRepository from "../repository/user.repository";
import OrderRepository from "../repository/order.repository";
import { Gender } from "../types/dto";

const userRepository = new UserRepository();
const orderRepository = new OrderRepository();
const userService = new UserService(userRepository, orderRepository);

describe("User Service", () => {
  it("should create user successfully", async () => {
    // Arrage
    jest
      .spyOn(UserRepository.prototype, "create")
      .mockImplementation(async (model) => {
        return {
          ...model,
          _id: "12345",
        };
      });
    jest
      .spyOn(OrderRepository.prototype, "sumAmountByUser")
      .mockImplementation(async () => 0);

    // Act
    const created = await userService.createUser({
      age: 10,
      gender: Gender.MALE,
      phone: "0388053223",
      full_name: "Quang",
    });

    // Assert
    expect(created).toEqual({
      _id: "12345",
      age: 10,
      gender: "male",
      phone: "0388053223",
      full_name: "Quang",
      total_amount: 0,
    });
  });

  it("should throw error when create user with invalid phone", async () => {
    // Act
    const create = userService.createUser({
      age: 10,
      gender: Gender.MALE,
      phone: "0123456",
      full_name: "Quang",
    });

    // Assert
    await expect(create).rejects.toThrow(
      "Invalid input: Phone must be a valid Vietnamese phone number"
    );
  });

  it("should update user successfully", async () => {
    // Arrage
    const userModel = {
      _id: "12345",
      age: 10,
      gender: Gender.MALE,
      phone: "0388053223",
      full_name: "Quang",
    };

    jest
      .spyOn(UserRepository.prototype, "update")
      .mockImplementation(async (model) => model);
    jest
      .spyOn(UserRepository.prototype, "findById")
      .mockImplementation(async (id) => userModel);
    jest
      .spyOn(OrderRepository.prototype, "sumAmountByUser")
      .mockImplementation(async () => 5);

    // Act
    const updated = await userService.updateUser(userModel);

    // Assert
    expect(updated).toEqual({
      ...userModel,
      total_amount: 5,
    });
  });

  it("should throw error when update non-existing user", async () => {
    // Arrage
    const userModel = {
      _id: "12345",
      age: 10,
      gender: Gender.MALE,
      phone: "0388053223",
      full_name: "Quang",
    };

    jest
      .spyOn(UserRepository.prototype, "findById")
      .mockImplementation(async (id) => null);

    // Act
    const update = userService.updateUser(userModel);

    // Assert
    await expect(update).rejects.toThrow("User not found");
  });

  it("should throw error when update user with invalid phone", async () => {
    // Arrage
    const userModel = {
      _id: "12345",
      age: 10,
      gender: Gender.MALE,
      phone: "008805323",
      full_name: "Quang",
    };

    // Act
    const update = userService.updateUser(userModel);

    // Assert
    await expect(update).rejects.toThrow(
      "Invalid input: Phone must be a valid Vietnamese phone number"
    );
  });
});
