import { IUserService } from "../types/service";
import { IOrderRepository, IUserRepository } from "../types/repository";
import { CreateUserDTO, UpdateUserDTO, UserResult } from "../types/dto";
import { UserModel } from "../types/model";
import {
  CreateUserValidator,
  UpdateUserValidator,
} from "../validator/user.validator";

export default class UserService implements IUserService {
  private repo: IUserRepository;
  private orderRepo: IOrderRepository;

  constructor(
    userRepository: IUserRepository,
    orderRepository: IOrderRepository
  ) {
    this.repo = userRepository;
    this.orderRepo = orderRepository;
  }

  /**
   * Create user
   * @param dto dto to create user
   * @returns User result
   * @throws {@link Error} when input is invalid
   */
  async createUser(dto: CreateUserDTO): Promise<UserResult> {
    const { error } = CreateUserValidator.validate(dto);
    if (error) {
      throw new Error("Invalid input: " + error.message);
    }

    const model: UserModel = {
      full_name: dto.full_name,
      age: dto.age,
      gender: dto.gender?.toLowerCase(),
      phone: dto.phone,
    };
    const created = await this.repo.create(model);
    return this.adaptUserResult(created);
  }

  /**
   * Update user
   * @param dto dto to updatee user
   * @returns User result
   * @throws {@link Error} when input is invalid or user is not existed
   */
  async updateUser(dto: UpdateUserDTO): Promise<UserResult> {
    const { error } = UpdateUserValidator.validate(dto);
    if (error) {
      throw new Error("Invalid input: " + error.message);
    }

    const existedUser = await this.assertUserById(dto._id);
    const modelToUpate = {
      ...existedUser,
      ...dto,
    };
    const updated = await this.repo.update(modelToUpate);
    return this.adaptUserResult(updated);
  }

  /**
   * Get user by id
   * @param id user's id
   * @returns User result
   * @throws {@link Error} when user is not existed
   */
  async getUser(id: string): Promise<UserResult> {
    const existedUser = await this.assertUserById(id);
    return this.adaptUserResult(existedUser);
  }

  /**
   * Assert and get user by id
   * @param id user's id
   * @returns User result
   * @throws {@link Error} when user is not existed
   */
  async assertUserById(id: string): Promise<UserModel> {
    const existedUser = await this.repo.findById(id);
    if (!existedUser) {
      throw new Error("User not found");
    }
    return existedUser;
  }

  /**
   * Adapt user model to user result
   * @param model user model
   * @returns User result
   */
  private async adaptUserResult(model: UserModel): Promise<UserResult> {
    const result: UserResult = {
      ...model,
      _id: model._id!,
      total_amount: await this.orderRepo.sumAmountByUser(model._id!),
    };
    return result;
  }
}
