const userMock = jest.mock("../repository/user.repository");
jest.mock("../repository/order.repository");

import OrderService from "./order.service";
import UserRepository from "../repository/user.repository";
import OrderRepository from "../repository/order.repository";
import { CreateOrderDTO, Gender } from "../types/dto";
import { OrderModel, UserModel } from "../types/model";

const userRepository = new UserRepository();
const orderRepository = new OrderRepository();
const orderService = new OrderService(orderRepository, userRepository);

describe("Order Service", () => {
  it("should calculate accrued amount correctlly", () => {
    // Arrage
    const amount = 100000;
    const interest_rate = 0.05;

    // Act
    const result = (OrderService as any).calculateAccruedAmount(
      amount,
      interest_rate
    );

    // Assert
    expect(result).toEqual([
      100417, 100835, 101255, 101677, 102101, 102526, 102953, 103382, 103813,
      104246, 104680, 105116,
    ]);
  });

  it("should return invalid code", async () => {
    // Arrage
    const code = "12345";
    const orderModel: OrderModel = {
      code,
      _id: "12345",
      amount: 10000,
      interest_rate: 0.02,
      user: "12345",
    };
    jest
      .spyOn(OrderRepository.prototype, "findByCode")
      .mockImplementation(async (code) => orderModel);

    // Act
    const isValid = await orderService.isValidCode(code);

    // Assert
    expect(isValid).toEqual(false);
  });

  it("should create order successfully", async () => {
    // Arrage
    const userModel: UserModel = {
      _id: "12345",
      age: 10,
      gender: Gender.MALE,
      phone: "008805323",
      full_name: "Quang",
    };
    const createDTO: CreateOrderDTO = {
      user: "1234",
      amount: 100000,
      interest_rate: 0.05,
    };

    jest
      .spyOn(UserRepository.prototype, "findById")
      .mockImplementation(async (id) => userModel);
    jest
      .spyOn(OrderRepository.prototype, "findByCode")
      .mockImplementation(async (code) => null);
    jest
      .spyOn(OrderRepository.prototype, "create")
      .mockImplementation(async (dto) => ({
        ...dto,
        _id: "001",
      }));
    (OrderService as any).generateCode = jest.fn().mockReturnValue("22222");

    // Act
    const created = await orderService.createOrder(createDTO);

    // Assert
    expect(created).toEqual({
      ...createDTO,
      _id: "001",
      code: "22222",
      accrued_amount: [
        100417, 100835, 101255, 101677, 102101, 102526, 102953, 103382, 103813,
        104246, 104680, 105116,
      ],
    });
  });

  it("should throw error when create order with non-existing user", async () => {
    // Arrage
    const createDTO: CreateOrderDTO = {
      user: "1234",
      amount: 100000,
      interest_rate: 0.05,
    };

    jest
      .spyOn(UserRepository.prototype, "findById")
      .mockImplementation(async (id) => null);

    // Act
    const create = orderService.createOrder(createDTO);

    // Assert
    expect(create).rejects.toThrow("User not found");
  });
});
