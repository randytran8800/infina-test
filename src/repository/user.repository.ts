import { UserModel } from "types/model";
import { IUserRepository } from "../types/repository";
import UserEntity from "../entity/user.entity";
import { Optional } from "types/base";

export default class UserRepository implements IUserRepository {
  async update(model: UserModel): Promise<UserModel> {
    let entity = new UserEntity(model);
    entity.isNew = false;
    entity = await entity.save();
    return entity.toObject();
  }

  async create(model: UserModel): Promise<UserModel> {
    let entity = new UserEntity(model);
    entity = await entity.save();
    return entity.toObject();
  }

  async findById(id: string): Promise<Optional<UserModel>> {
    const result = await UserEntity.findById(id).exec();
    return result?._doc;
  }
}
