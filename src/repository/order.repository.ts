import { Optional } from "types/base";
import { OrderModel } from "types/model";
import { IOrderRepository } from "types/repository";
import OrderEntity from "../entity/order.entity";

export default class OrderRepository implements IOrderRepository {
  async findByCode(code: string): Promise<Optional<OrderModel>> {
    const result = await OrderEntity.findOne({ code }).exec();
    return result?._doc;
  }

  async findById(id: string): Promise<Optional<OrderModel>> {
    const result = await OrderEntity.findById(id).exec();
    return result?._doc;
  }

  async findAllByUser(userId: string): Promise<OrderModel[]> {
    const result = await OrderEntity.find({ user: userId }).exec();
    return result.map((res) => res._doc);
  }

  async update(model: OrderModel): Promise<OrderModel> {
    let entity = new OrderEntity(model);
    entity.isNew = false;
    entity = await entity.save();
    return entity.toObject();
  }

  async create(model: OrderModel): Promise<OrderModel> {
    let entity = new OrderEntity(model);
    entity = await entity.save();
    return entity.toObject();
  }

  /**
   * Get total amount of user's orders
   * @param userId user's id
   * @returns total amount
   */
  async sumAmountByUser(userId: string): Promise<number> {
    const result = await OrderEntity.aggregate([
      { $match: { user: userId } },
      { $group: { _id: null, sum: { $sum: "$amount" } } },
    ]).exec();
    return result[0]?.sum || 0;
  }
}
