import express, { Express, Request, Response } from "express";
import { graphqlHTTP } from "express-graphql";
import { buildSchema } from "graphql";
import { readFileSync } from "fs";
import initRoot from "./graphql/root";
import { initMongo } from "./util/mongo";
import { initEnv } from "./util/env";
import { join } from "path";

export async function initApp() {
  initEnv();
  await initMongo();

  const app: Express = express();
  const port = process.env.PORT || 3000;

  app.use(
    "/graphql",
    graphqlHTTP({
      schema: buildSchema(
        readFileSync(join(__dirname, "./graphql/schema.graphql")).toString()
      ),
      rootValue: initRoot(),
      graphiql: true,
    })
  );

  app.get("/", (req: Request, res: Response) => {
    res.send("Welcome");
  });

  app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
  });
}
