import mongoose from "mongoose";

export async function initMongo() {
  const mongoUri = process.env.MONGO_URI || "";
  try {
    await mongoose.connect(mongoUri);
    return console.log("MongoDB Connected");
  } catch (err) {
    return console.log(err);
  }
}
