import dotenv from "dotenv";
import { join } from "path";

export function initEnv() {
  const env = process.env.ENVIRONMENT || "dev";
  dotenv.config({
    path: join(__dirname, `../environment/.env.${env.toLowerCase()}`),
  });
}
