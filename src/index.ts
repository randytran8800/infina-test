import { initApp } from "./app";

initApp().catch((err) => {
  console.error(err);
});
