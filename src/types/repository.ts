import { Optional } from "./base";
import { OrderModel, UserModel } from "./model";

export interface IRepository<Model> {
  update(model: Model): Promise<Model>;
  create(model: Model): Promise<Model>;
  findById(id: string): Promise<Optional<Model>>;
}
export interface IUserRepository extends IRepository<UserModel> {}

export interface IOrderRepository extends IRepository<OrderModel> {
  findAllByUser(userId: string): Promise<OrderModel[]>;
  findByCode(code: string): Promise<Optional<OrderModel>>;
  sumAmountByUser(userId: string): Promise<number>;
}
