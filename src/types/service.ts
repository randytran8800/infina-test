import {
  CreateOrderDTO,
  CreateUserDTO,
  OrderResult,
  UpdateUserDTO,
  UserResult,
} from "./dto";

export interface IUserService {
  getUser(id: string): Promise<UserResult>;
  createUser(dto: CreateUserDTO): Promise<UserResult>;
  updateUser(dto: UpdateUserDTO): Promise<UserResult>;
}

export interface IOrderService {
  getOrder(id: string): Promise<OrderResult>;
  getOrdersByUser(userId: string): Promise<OrderResult[]>;
  createOrder(dto: CreateOrderDTO): Promise<OrderResult>;
}
