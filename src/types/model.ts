export type UserModel = {
  _id?: string;
  full_name: string;
  phone: string;
  age: number;
  gender: string;
};

export type OrderModel = {
  _id?: string;
  user: string;
  code: string;
  amount: number;
  interest_rate: number;
};
