import { CreateOrderDTO, CreateUserDTO, UpdateUserDTO } from "./dto";

export type CreateUserInput = CreateUserDTO;
export type UpdateUserInput = UpdateUserDTO;

export type CreateOrderInput = CreateOrderDTO;
