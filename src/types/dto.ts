export enum Gender {
  MALE = "MALE",
  FEMALE = "FEMALE",
  OTHER = "OTHER",
}

export type UserResult = {
  _id: string;
  full_name: string;
  phone: string;
  age: number;
  gender: string;
  total_amount: number;
};

export type CreateUserDTO = {
  full_name: string;
  phone: string;
  age: number;
  gender: Gender;
};

export type UpdateUserDTO = CreateUserDTO & {
  _id: string;
};

export type OrderResult = {
  _id: string;
  user: string;
  code: string;
  amount: number;
  interest_rate: number;
  accrued_amount: number[];
};

export type CreateOrderDTO = {
  user: string;
  amount: number;
  interest_rate: number;
};
