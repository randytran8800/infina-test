import Joi from "joi";

export const CreateOrderValidator = Joi.object({
  user: Joi.string().required(),

  amount: Joi.number().required(),

  interest_rate: Joi.number().required(),
});
