import Joi from "joi";

export const CreateUserValidator = Joi.object({
  full_name: Joi.string().required(),

  phone: Joi.string()
    .pattern(new RegExp("^(84|0[3|5|7|8|9])+([0-9]{8,9})$"))
    .required()
    .error(new Error("Phone must be a valid Vietnamese phone number")),

  age: Joi.number()
    .max(200)
    .min(5)
    .error(new Error("Age must be between 5 an 200")),

  gender: Joi.string()
    .pattern(new RegExp("MALE|FEMALE|OTHER"))
    .error(new Error("Invalid gender")),
});

export const UpdateUserValidator = CreateUserValidator.keys({
  _id: Joi.string().required(),
});
