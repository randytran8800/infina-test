import mongoose from "mongoose";
const { Schema } = mongoose;

const Order = new Schema({
  user: { type: Schema.Types.ObjectId, required: true },
  code: { type: String, unique: true, required: true, dropDups: true },
  amount: { type: Number, required: true },
  interest_rate: { type: Number, required: true },
});

export default mongoose.model("orders", Order);
