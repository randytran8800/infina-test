import mongoose from "mongoose";
const { Schema } = mongoose;

const User = new Schema({
  full_name: { type: String, required: true },
  phone: { type: String, required: true },
  age: Number,
  gender: { type: String, required: true },
});

export default mongoose.model("users", User);
