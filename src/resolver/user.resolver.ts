import { UserResult } from "../types/dto";
import { CreateUserInput, UpdateUserInput } from "../types/input";
import { IUserService } from "../types/service";

export default class UserResolver {
  private userService: IUserService;

  constructor(userService: IUserService) {
    this.userService = userService;
  }

  createUser({ input }: { input: CreateUserInput }): Promise<UserResult> {
    return this.userService.createUser(input);
  }

  updateUser({ input }: { input: UpdateUserInput }): Promise<UserResult> {
    return this.userService.updateUser(input);
  }

  async getUser({ id }: { id: string }): Promise<UserResult> {
    const test = await this.userService.getUser(id);
    return test;
  }
}
