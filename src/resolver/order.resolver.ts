import { OrderResult } from "types/dto";
import { CreateOrderInput } from "types/input";
import { IOrderService } from "types/service";

export default class OrderResolver {
  private orderService: IOrderService;

  constructor(orderService: IOrderService) {
    this.orderService = orderService;
  }

  createOrder({ input }: { input: CreateOrderInput }): Promise<OrderResult> {
    return this.orderService.createOrder(input);
  }

  getOrder({ id }: { id: string }): Promise<OrderResult> {
    return this.orderService.getOrder(id);
  }

  getOrders({ userId }: { userId: string }): Promise<OrderResult[]> {
    return this.orderService.getOrdersByUser(userId);
  }
}
