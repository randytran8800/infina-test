# Start project
### For Production
```sh
docker-compose up
```
Server will listen on port 3000 by default and GraphiQL will available at [localhost:3000/graphql](localhost:3000/graphql)
### For Development
```sh
npm i
npm run dev
```

### Unit tests
```sh
npm run test:unit
```

# Environments
Edit environment at this folder `src/environment/.env.*`


# APIs
### Get user by id
```
getUser(id: String!): UserResult!
```
### Get order by id
```
getOrder(id: String!): OrderResult!
```
### Get all orders of user
```
getOrders(userId: String!): [OrderResult!]

```
### Create user
```
createUser(input: CreateUserInput!): UserResult

```
### Update user info
```
updateUser(input: UpdateUserInput!): UserResult

```
### Create order
```
createOrder(input: CreateOrderInput!): OrderResult

```
## Input
### CreateUserInput
```
input CreateUserInput {
    full_name: String!
    phone: String!
    age: Int
    gender: Gender!
}
enum Gender {
    MALE
    FEMALE
}
```
### UpdateUserInput
```
input UpdateUserInput {
    _id: String!
    full_name: String!
    phone: String!
    age: Int
    gender: Gender!
}
```
### CreateOrderInput
```
input CreateOrderInput {
    user: String!
    amount: Int!
    interest_rate: Float!
}
```
## Result
### UserResult
```
type UserResult {
    _id: String!
    full_name: String
    phone: String
    age: Int
    gender: String
    total_amount: Int
}
```
### OrderResult
```
type OrderResult {
    _id: String!
    user: String!
    amount: Int!
    code: String!
    interest_rate: Float!
    accrued_amount: [Int]
}
```

